package com.cogip.stockapp.utils

import com.cogip.stockapp.data.models.DateTime
import com.cogip.stockapp.utils.DateUtils.Companion.dateTimeToString
import org.junit.Assert
import org.junit.Test

class DateUtilsTest {
    @Test
    fun dateTimeToString_ReturnsEmptyIfNullParam() {
        Assert.assertTrue(dateTimeToString(null) == "")
    }
    @Test
    fun dateTimeToString_ReturnsEmptyIfInvalidParam() {
        val regex = "^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}$".toRegex()
        Assert.assertTrue(regex.containsMatchIn(dateTimeToString(DateTime(123))))
    }
}