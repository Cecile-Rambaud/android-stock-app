package com.cogip.stockapp.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cogip.stockapp.R
import com.cogip.stockapp.data.ApiClient
import com.cogip.stockapp.data.models.Product
import com.cogip.stockapp.data.models.Zone
import com.cogip.stockapp.productList.InventoryProductsListViewModel
import com.cogip.stockapp.productList.ProductsAdapter
import com.cogip.stockapp.utils.SessionManager

class ProductsActivity : AppCompatActivity() {

    private lateinit var apiClient: ApiClient
    private lateinit var productsList: RecyclerView
    private lateinit var listError: TextView
    private lateinit var loadingView: View

    lateinit var productsViewModel: InventoryProductsListViewModel
    private val productsAdapter = ProductsAdapter ({ product -> productsAdapterOnClick(product)}, arrayListOf())

    private var inventoryId = 0
    private var zoneId = 0
    private var zoneName = ""

    companion object {
        private const val EXTRA_ZONE_ID = "EXTRA_ZONE_ID"
        private const val EXTRA_ZONE_NAME = "EXTRA_ZONE_NAME"
        private const val EXTRA_INVENTORY_ID = "EXTRA_INVENTORY_ID"

        fun createIntent(context: Context, inventoryId: Int, zone: Zone) : Intent {
            val intent = Intent(context, ProductsActivity()::class.java)
            intent.putExtra(EXTRA_INVENTORY_ID, inventoryId)
            intent.putExtra(EXTRA_ZONE_ID, zone.id)
            intent.putExtra(EXTRA_ZONE_NAME, zone.name)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        getDataFromIntent(intent)

        title = getString(R.string.inventory_detail_title, inventoryId) + " " + getString(R.string.zone_title, zoneName)

        apiClient = ApiClient(this.application)
        productsViewModel = ViewModelProvider(this).get(InventoryProductsListViewModel::class.java)


        listError = findViewById(R.id.listError)
        loadingView = findViewById(R.id.loadingView)
        productsList = findViewById(R.id.productsList)


        productsList.apply {
            layoutManager = LinearLayoutManager(this@ProductsActivity)
            adapter = productsAdapter
        }
        observeProductsViewModel()

        val fab: View = findViewById(R.id.fab)
        fab.setOnClickListener {
            fabOnClick()
        }

        if (SessionManager(this.application).fetchAuthToken().isNullOrEmpty()) {
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        refresh()
    }

   private fun refresh() {
        productsViewModel.refresh(apiClient.apiService, inventoryId, zoneId)
   }

    private fun getDataFromIntent(_intent: Intent?) {
        if (_intent == null)
            return
        inventoryId = _intent.getIntExtra(EXTRA_INVENTORY_ID, 0)
        zoneId = _intent.getIntExtra(EXTRA_ZONE_ID, 0)
        zoneName = _intent.getStringExtra(EXTRA_ZONE_NAME) ?: ""
    }


    private fun observeProductsViewModel() {
        productsViewModel.inventoryProducts.observe(this) { products ->
            products?.let {
                productsList.visibility = View.VISIBLE
                productsAdapter.updateForDataChanges(it)
            }
        }
        productsViewModel.productsLoadError.observe(this) { isError ->
            listError.visibility = if (isError == "") View.GONE else View.VISIBLE
            listError.text = isError
        }
        productsViewModel.loading.observe(this) { isLoading ->
            isLoading?.let {
                loadingView.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    listError.visibility = View.GONE
                    productsList.visibility = View.GONE
                }
            }
        }
    }


    /* Adds products to productList when FAB is clicked. */
    private fun fabOnClick() {
        val intent = Intent(this@ProductsActivity, CameraXLivePreviewActivity::class.java)
        intent.putExtra(CameraXLivePreviewActivity.EXTRA_ZONE_ID, zoneId)
        intent.putExtra(CameraXLivePreviewActivity.EXTRA_INVENTORY_ID, inventoryId)
        startActivity(intent)
    }

    /* Opens ProductDetailActivity when RecyclerView item is clicked. */
    private fun productsAdapterOnClick(product: Product) {
//        val intent = Intent(this, ProductDetailActivity()::class.java)
//        intent.putExtra(PRODUCT_ID, product.id)
//        startActivity(intent)
    }
}
