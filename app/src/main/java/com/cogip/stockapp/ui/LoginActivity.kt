package com.cogip.stockapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.cogip.stockapp.R
import com.cogip.stockapp.data.ApiClient
import com.cogip.stockapp.data.requests.LoginRequest
import com.cogip.stockapp.data.responses.LoginResponse
import com.cogip.stockapp.utils.Constants
import com.cogip.stockapp.utils.SessionManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var sessionManager: SessionManager
    private lateinit var apiClient: ApiClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        findViewById<View>(R.id.btnLogin).setOnClickListener(this)
        apiClient = ApiClient(this.application)
        sessionManager = SessionManager(this.application)
    }

    fun goToNextScreen() {
        this@LoginActivity.finish()
    }

    override fun onClick(view: View?) {
        if (view?.id == R.id.btnLogin) {
            val username = findViewById<EditText>(R.id.actLoginUsernameEditText).text.toString()
            val password = findViewById<EditText>(R.id.actLoginPasswordEditText).text.toString()
            if (username.isEmpty() || password.isEmpty()) {
                val toast = Toast.makeText(this@LoginActivity, getString(R.string.login_password_must_enter), Toast.LENGTH_LONG)
                toast.show()
                return
            } else {
                doLogin(username, password)
            }
        }
    }

    private fun doLogin(username: String, password: String) {
        apiClient.apiService.login(LoginRequest(username, password))
            .enqueue(object : Callback<LoginResponse> {
                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    val toast = Toast.makeText(this@LoginActivity, t.message, Toast.LENGTH_LONG)
                    Log.e(Constants.APP_LOG_TAG, "login failure", t)
                    toast.show()
                }

                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                    val loginResponse = response.body()
                    if (loginResponse?.authToken != null) {
                        sessionManager.saveAccessTokens(loginResponse.authToken, loginResponse.refreshToken)
                        //TODO : decode JWT TOKEN to have username
                        val toast = Toast.makeText(this@LoginActivity, loginResponse.authToken, Toast.LENGTH_LONG)
                        toast.show()
                        goToNextScreen()
                    } else {
                        // Error logging in
                        val toast = Toast.makeText(this@LoginActivity, response.errorBody()?.string(), Toast.LENGTH_LONG)
                        toast.show()
                        Log.d(Constants.APP_LOG_TAG, "login onResponse error : "+ response.raw().code + " " + response.raw().message)
                    }
                }
            })

    }
}
