package com.cogip.stockapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.cogip.stockapp.R
import com.cogip.stockapp.data.ApiClient
import com.cogip.stockapp.productList.InventoryAddProductsViewModel

class AddProductDialogFragment : DialogFragment(), View.OnClickListener {
    private lateinit var apiClient: ApiClient

    private var inventoryId = 0
    private var zoneId = 0
    private lateinit var loadingView: View
    private lateinit var listError: TextView
    private lateinit var barcodeET: EditText
    private lateinit var addBtn: Button

    private lateinit var addProductsViewModel: InventoryAddProductsViewModel


    companion object {
        fun newInstance(zoneId: Int, inventoryId: Int): AddProductDialogFragment {
            val frag = AddProductDialogFragment()
            val args = Bundle()
            args.putInt(InventoryDetailActivity.EXTRA_INVENTORY_ID, inventoryId)
            args.putInt(InventoryDetailActivity.EXTRA_ZONE_ID, zoneId)
            frag.arguments = args
            return frag
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_add_product, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addProductsViewModel = ViewModelProvider(this).get(InventoryAddProductsViewModel::class.java)

        dialog?.setTitle(getString(R.string.add_product_title))
        listError = view.findViewById(R.id.listError)
        loadingView = view.findViewById(R.id.loadingView)
        barcodeET = view.findViewById(R.id.actAddProductBarcodeET)
        addBtn = view.findViewById(R.id.actAddProductAddBtn)
        getDataFromArgument()

        activity?.let {
            apiClient = ApiClient(it.application)
        }
        observeViewModel()

        addBtn.setOnClickListener(this)
        barcodeET.requestFocus()
    }

    private fun observeViewModel() {
        addProductsViewModel.inventoryProductAdded.observe(this) { inventoryProducts ->
            if (inventoryProducts != null) {
                Toast.makeText(
                    context,
                    "Product ${inventoryProducts.product.name} added in zone ${inventoryProducts.zone.name}(${inventoryProducts.zone.id}) of inventory zone ${inventoryProducts.inventory.id}",
                    Toast.LENGTH_LONG
                ).show()
                dismiss()
            }
            addBtn.isEnabled = true
        }
        addProductsViewModel.productsAddError.observe(this) { isError ->
            listError.visibility = if (isError == "") View.GONE else View.VISIBLE
            listError.text = isError
        }
        addProductsViewModel.addingProduct.observe(this) { isLoading ->
            isLoading?.let {
                addBtn.isEnabled = !it
                loadingView.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    listError.visibility = View.GONE
                }
            }
        }
    }

    private fun getDataFromArgument() {
        arguments?.let {
            inventoryId = it.getInt(InventoryDetailActivity.EXTRA_INVENTORY_ID, 0)
            zoneId = it.getInt(InventoryDetailActivity.EXTRA_ZONE_ID, 0)
        }
    }

    override fun onClick(view: View?) {
        if (view?.id == R.id.actAddProductAddBtn) {
           val barcode =  barcodeET.text
            if (barcode.isNullOrEmpty()) {
                Toast.makeText(context, "Please enter a barcode", Toast.LENGTH_LONG).show()
                return
            }
            addProductsViewModel.addProduct(apiClient.apiService, inventoryId, zoneId, barcode.toString())
        }
    }

}