package com.cogip.stockapp.ui

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import com.cogip.stockapp.R


class MessageBroadcastReceiver : BroadcastReceiver() {
    companion object {
        const val ACTION_MESSAGE_TO_DISPLAY = "DISPLAY_MESSAGE"
        const val EXTRA_MESSAGE = "EXTRA_MESSAGE"
    }

    override fun onReceive(context: Context?, intent: Intent?)
    {
        intent?.let {
            if (!it.action.equals(ACTION_MESSAGE_TO_DISPLAY))
                return
            val message = it.extras?.getString(EXTRA_MESSAGE)
            context?.apply {
                val builder = AlertDialog.Builder(this)
                builder.setTitle(this.getString(R.string.error))
                builder.setMessage(message)
                builder.show()
            }
        }


    }
}