package com.cogip.stockapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cogip.stockapp.R
import com.cogip.stockapp.data.ApiClient
import com.cogip.stockapp.inventoryList.InventoriesAdapter
import com.cogip.stockapp.inventoryList.InventoriesListViewModel
import com.cogip.stockapp.utils.SessionManager

class InventoriesActivity : AppCompatActivity() {
    private lateinit var apiClient: ApiClient
    private lateinit var inventoriesList: RecyclerView
    private lateinit var listError: TextView
    private lateinit var loadingView: View
    private lateinit var inventoriesListViewModel: InventoriesListViewModel
    private val inventoriesAdapter = InventoriesAdapter (
        InventoriesAdapter.InventoryListener { inventoryId -> inventoriesAdapterOnClick(inventoryId) }, arrayListOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inventories)
        title = getString(R.string.inventories)

        apiClient = ApiClient(this.application)
        inventoriesListViewModel = ViewModelProvider(this).get(InventoriesListViewModel::class.java)
        inventoriesListViewModel.refresh(apiClient.apiService)

        listError = findViewById(R.id.listError)
        loadingView = findViewById(R.id.loadingView)
        inventoriesList = findViewById(R.id.inventoriesList)


        inventoriesList.apply {
            layoutManager = LinearLayoutManager(this@InventoriesActivity, RecyclerView.VERTICAL, false)
            adapter = inventoriesAdapter
        }
        observeInventoriesViewModel()

        if (SessionManager(this.application).fetchAuthToken().isNullOrEmpty()) {
            finish()
        }
    }

    private fun observeInventoriesViewModel() {
        inventoriesListViewModel.inventories.observe(this) { inventories ->
            inventories?.let {
                inventoriesList.visibility = View.VISIBLE
                inventoriesAdapter.updateForDataChanges(it)
            }
        }
        inventoriesListViewModel.inventoriesLoadError.observe(this) { isError ->
            listError.visibility = if (isError == "") View.GONE else View.VISIBLE
            listError.text = isError
        }
        inventoriesListViewModel.loading.observe(this) { isLoading ->
            isLoading?.let {
                loadingView.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    listError.visibility = View.GONE
                    inventoriesList.visibility = View.GONE
                }
            }
        }
    }

    /* Opens InventoryDetailActivity when RecyclerView item is clicked. */
    private fun inventoriesAdapterOnClick(inventoryId: Int) {
        val intent = Intent(this@InventoriesActivity, InventoryDetailActivity::class.java)
        intent.putExtra(InventoryDetailActivity.EXTRA_INVENTORY_ID, inventoryId)
        startActivity(intent)
    }

    override fun onBackPressed() {
        SessionManager(this.application).removeTokens()
        super.onBackPressed()
    }
}
