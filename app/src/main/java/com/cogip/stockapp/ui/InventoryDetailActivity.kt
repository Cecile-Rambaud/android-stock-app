package com.cogip.stockapp.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cogip.stockapp.R
import com.cogip.stockapp.data.ApiClient
import com.cogip.stockapp.data.models.Zone
import com.cogip.stockapp.inventoryDetail.InventoryDetailViewModel
import com.cogip.stockapp.inventoryDetail.ZonesAdapter
import com.cogip.stockapp.utils.DateUtils

class InventoryDetailActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_INVENTORY_ID = "EXTRA_INVENTORY_ID"
        const val EXTRA_ZONE_ID = "EXTRA_ZONE_ID"
    }

    private lateinit var errorTV: TextView
    private lateinit var stateTV: TextView
    private lateinit var startDateTV: TextView
    private lateinit var endDateTV: TextView
    private lateinit var loadingPb: ProgressBar
    private lateinit var zonesRv: RecyclerView


    var inventoryId: Int = 0
    private lateinit var apiClient: ApiClient
    private lateinit var mViewModel: InventoryDetailViewModel
    private val zonesAdapter = ZonesAdapter({ zone -> zonesAdapterOnClick(zone) }, arrayListOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inventory_detail)

        inventoryId = getInventoryIdFromIntent(intent)
        if (inventoryId <= 0) {
            Toast.makeText(
                this,
                "Error : No $EXTRA_INVENTORY_ID found",
                Toast.LENGTH_LONG
            ).show()
            finish()
        }


        errorTV = findViewById(R.id.actInventoryDetailErrorTv)
        stateTV = findViewById(R.id.actInventoryDetailStateTv)
        startDateTV = findViewById(R.id.actInventoryDetailStartDateTv)
        endDateTV = findViewById(R.id.actInventoryDetailEndDateTv)
        stateTV = findViewById(R.id.actInventoryDetailStateTv)
        loadingPb = findViewById(R.id.actInventoryDetailLoadingPb)
        zonesRv = findViewById(R.id.actInventoryDetailZonesRv)
        title = getString(R.string.inventory_detail_title, inventoryId)

        apiClient = ApiClient(this.application)
        mViewModel = ViewModelProvider(this).get(InventoryDetailViewModel::class.java)
        mViewModel.refresh(apiClient.apiService, inventoryId)

        zonesRv.apply {
            layoutManager = LinearLayoutManager(this@InventoryDetailActivity)
            adapter = zonesAdapter
        }
        observeViewModel()
    }

    private fun getInventoryIdFromIntent(_intent: Intent?): Int {
        if (_intent == null)
            return 0
        return _intent.getIntExtra(EXTRA_INVENTORY_ID, 0)
    }

    @SuppressLint("StringFormatMatches")
    private fun observeViewModel() {
        mViewModel.inventory.observe(this) { inventory ->
            inventory?.let {
                stateTV.text = getString(R.string.inventory_detail_state, "${it.state}")
                startDateTV.text = getString(
                    R.string.inventory_detail_date_start,
                    DateUtils.dateTimeToString(it.startDate)
                )
                endDateTV.text = getString(
                    R.string.inventory_detail_date_end,
                    DateUtils.dateTimeToString(it.endDate)
                )
            }
        }
        mViewModel.zones.observe(this) { inventory ->
            inventory?.let {
                zonesAdapter.updateForDataChanges(it)
            }
        }
        mViewModel.inventoryLoadError.observe(this) { isError ->
            errorTV.visibility = if (isError == "") View.GONE else View.VISIBLE
            errorTV.text = isError
        }
        mViewModel.loading.observe(this) { isLoading ->
            isLoading?.let {
                loadingPb.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    errorTV.visibility = View.GONE
                }
            }
        }
    }

    private fun zonesAdapterOnClick(zone: Zone) {
        startActivity(ProductsActivity.createIntent(this, inventoryId, zone))
    }
}