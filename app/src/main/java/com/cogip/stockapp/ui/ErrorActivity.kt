package com.cogip.stockapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.cogip.stockapp.R
import com.cogip.stockapp.utils.Constants


class ErrorActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_error)
        findViewById<View>(R.id.error_message_tv).setOnClickListener(this)
        displayMessageFromIntent()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        displayMessageFromIntent()
    }

    private fun displayMessageFromIntent() {
        var value = "Error"
        if (intent.hasExtra(Constants.INTENT_KEY_ERROR))
            value = intent.getStringExtra(Constants.INTENT_KEY_ERROR).toString()
        findViewById<TextView>(R.id.error_message_tv).text = value
    }


    override fun onClick(view: View?) {
        finish()
    }
}
