/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cogip.stockapp.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.cogip.stockapp.R
import com.cogip.stockapp.barcodescanner.BarcodeScannerProcessor
import com.cogip.stockapp.barcodescanner.CameraXViewModel
import com.cogip.stockapp.barcodescanner.GraphicOverlay
import com.cogip.stockapp.data.ApiClient
import com.cogip.stockapp.productList.InventoryAddProductsViewModel
import com.cogip.stockapp.utils.PreferenceUtils
import com.google.android.gms.common.annotation.KeepName
import com.google.mlkit.common.MlKitException
import com.google.mlkit.vision.barcode.common.Barcode


/** Live preview demo app for ML Kit APIs using CameraX. */
@KeepName
@RequiresApi(VERSION_CODES.LOLLIPOP)
class CameraXLivePreviewActivity :
  AppCompatActivity(),
  ActivityCompat.OnRequestPermissionsResultCallback,
  CompoundButton.OnCheckedChangeListener,
  BarcodeScannerProcessor.BarcodeScannerSuccessListener {
  private lateinit var apiClient: ApiClient
  private lateinit var addProductsViewModel: InventoryAddProductsViewModel
  private var previewView: PreviewView? = null
  private var graphicOverlay: GraphicOverlay? = null
  private var cameraProvider: ProcessCameraProvider? = null
  private var previewUseCase: Preview? = null
  private var analysisUseCase: ImageAnalysis? = null
  private var imageProcessor: BarcodeScannerProcessor? = null
  private var needUpdateGraphicOverlayImageSourceInfo = false
  private var lensFacing = CameraSelector.LENS_FACING_BACK
  private var cameraSelector: CameraSelector? = null

  private var inventoryId = 0
  private var zoneId = 0



  @SuppressLint("UnsafeOptInUsageError")
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    Log.d(TAG, "onCreate")

    getDataFromIntent(intent)
    apiClient = ApiClient(application)
    addProductsViewModel = ViewModelProvider(this).get(InventoryAddProductsViewModel::class.java)
    observeViewModel()
    cameraSelector = CameraSelector.Builder().requireLensFacing(lensFacing).build()
    setContentView(R.layout.activity_vision_camerax_live_preview)
    previewView = findViewById(R.id.preview_view)
    if (previewView == null) {
      Log.d(TAG, "previewView is null")
    }
    graphicOverlay = findViewById(R.id.graphic_overlay)
    if (graphicOverlay == null) {
      Log.d(TAG, "graphicOverlay is null")
    }



    findViewById<Button>(R.id.manual_entry_btn).setOnClickListener {
      displayAddProductDialogFragment()
    }

    val facingSwitch = findViewById<ToggleButton>(R.id.facing_switch)
    facingSwitch.setOnCheckedChangeListener(this)
    ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(application))
      .get(CameraXViewModel::class.java)
      .processCameraProvider
      .observe(
        this
      ) { provider: ProcessCameraProvider? ->
        cameraProvider = provider
        if (allPermissionsGranted()) {
          bindAllCameraUseCases()
        }
      }

    val settingsButton = findViewById<ImageView>(R.id.settings_button)
    settingsButton.setOnClickListener {
      val intent = Intent(applicationContext, SettingsActivity::class.java)
      startActivity(intent)
    }

    if (!allPermissionsGranted()) {
      runtimePermissions
    }
  }

  private fun getDataFromIntent(_intent: Intent?) {
    if (_intent == null)
      return
    inventoryId = _intent.getIntExtra(EXTRA_INVENTORY_ID, 0)
    zoneId = _intent.getIntExtra(EXTRA_ZONE_ID, 0)
  }

  private fun observeViewModel() {
    addProductsViewModel.inventoryProductAdded.observe(this) { inventoryProducts ->
      if (inventoryProducts != null) {
        Toast.makeText(
          this,
          "Product ${inventoryProducts.product.name} added in zone ${inventoryProducts.zone.name}(${inventoryProducts.zone.id}) of inventory zone ${inventoryProducts.inventory.id}",
          Toast.LENGTH_LONG
        ).show()
        finish()
      }
    }
    addProductsViewModel.productsAddError.observe(this) {
      if (it != null && it.length > 0)
        Toast.makeText(this, "Error ${it}", Toast.LENGTH_LONG).show()
    }

    addProductsViewModel.addingProduct.observe(this) { isLoading ->
      isLoading?.let {
        //loadingView.visibility = if(it) View.VISIBLE else View.GONE
      }
    }
  }


  @SuppressLint("UnsafeOptInUsageError")
  override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
    if (cameraProvider == null) {
      return
    }
    val newLensFacing =
      if (lensFacing == CameraSelector.LENS_FACING_FRONT) {
        CameraSelector.LENS_FACING_BACK
      } else {
        CameraSelector.LENS_FACING_FRONT
      }
    val newCameraSelector = CameraSelector.Builder().requireLensFacing(newLensFacing).build()
    try {
      if (cameraProvider!!.hasCamera(newCameraSelector)) {
        Log.d(TAG, "Set facing to $newLensFacing")
        lensFacing = newLensFacing
        cameraSelector = newCameraSelector
        bindAllCameraUseCases()
        return
      }
    } catch (e: CameraInfoUnavailableException) {
      // Falls through
    }
    Toast.makeText(
        applicationContext,
        "This device does not have lens with facing: $newLensFacing",
        Toast.LENGTH_SHORT
      )
      .show()
  }

  @SuppressLint("UnsafeOptInUsageError")
  public override fun onResume() {
    super.onResume()
    bindAllCameraUseCases()
  }

  override fun onPause() {
    super.onPause()

    imageProcessor?.run { this.stop() }
  }

  public override fun onDestroy() {
    super.onDestroy()
    imageProcessor?.run { this.stop() }
  }

  @ExperimentalGetImage
  private fun bindAllCameraUseCases() {
    if (cameraProvider != null) {
      // As required by CameraX API, unbinds all use cases before trying to re-bind any of them.
      cameraProvider!!.unbindAll()
      bindPreviewUseCase()
      bindAnalysisUseCase()
    }
  }

  private fun bindPreviewUseCase() {
    if (!PreferenceUtils.isCameraLiveViewportEnabled(this)) {
      return
    }
    if (cameraProvider == null) {
      return
    }
    if (previewUseCase != null) {
      cameraProvider!!.unbind(previewUseCase)
    }

    val builder = Preview.Builder()
    val targetResolution = PreferenceUtils.getCameraXTargetResolution(this, lensFacing)
    if (targetResolution != null) {
      builder.setTargetResolution(targetResolution)
    }
    previewUseCase = builder.build()
    previewUseCase!!.setSurfaceProvider(previewView!!.surfaceProvider)
    cameraProvider!!.bindToLifecycle(/* lifecycleOwner= */ this, cameraSelector!!, previewUseCase)
  }

  @ExperimentalGetImage
  private fun bindAnalysisUseCase() {
    if (cameraProvider == null) {
      return
    }
    if (analysisUseCase != null) {
      cameraProvider!!.unbind(analysisUseCase)
    }
    if (imageProcessor != null) {
      imageProcessor!!.stop()
    }
    imageProcessor =
      try {
        Log.i(TAG, "Using Barcode Detector Processor")
        BarcodeScannerProcessor(this, this)
      } catch (e: Exception) {
        Log.e(TAG, "Can not create image processor", e)
        Toast.makeText(
            applicationContext,
            "Can not create image processor: " + e.localizedMessage,
            Toast.LENGTH_LONG
          )
          .show()
        return
      }

    val builder = ImageAnalysis.Builder()
    val targetResolution = PreferenceUtils.getCameraXTargetResolution(this, lensFacing)
    if (targetResolution != null) {
      builder.setTargetResolution(targetResolution)
    }
    analysisUseCase = builder.build()

    needUpdateGraphicOverlayImageSourceInfo = true

    analysisUseCase?.setAnalyzer(
      // imageProcessor.processImageProxy will use another thread to run the detection underneath,
      // thus we can just runs the analyzer itself on main thread.
      ContextCompat.getMainExecutor(this),
      { imageProxy: ImageProxy ->
        if (needUpdateGraphicOverlayImageSourceInfo) {
          val isImageFlipped = lensFacing == CameraSelector.LENS_FACING_FRONT
          val rotationDegrees = imageProxy.imageInfo.rotationDegrees
          if (rotationDegrees == 0 || rotationDegrees == 180) {
            graphicOverlay?.setImageSourceInfo(imageProxy.width, imageProxy.height, isImageFlipped)
          } else {
            graphicOverlay?.setImageSourceInfo(imageProxy.height, imageProxy.width, isImageFlipped)
          }
          needUpdateGraphicOverlayImageSourceInfo = false
        }
        try {
          graphicOverlay?.apply {

            imageProcessor?.processImageProxy(imageProxy, this)
          }

        } catch (e: MlKitException) {
          Log.e(TAG, "Failed to process image. Error: " + e.localizedMessage)
          Toast.makeText(applicationContext, e.localizedMessage, Toast.LENGTH_SHORT).show()
        }
      }
    )
    cameraProvider!!.bindToLifecycle(/* lifecycleOwner= */ this, cameraSelector!!, analysisUseCase)
  }

  private val requiredPermissions: Array<String?>
    get() =
      try {
        val info =
          this.packageManager.getPackageInfo(this.packageName, PackageManager.GET_PERMISSIONS)
        val ps = info.requestedPermissions
        if (ps != null && ps.isNotEmpty()) {
          ps
        } else {
          arrayOfNulls(0)
        }
      } catch (e: Exception) {
        arrayOfNulls(0)
      }

  private fun allPermissionsGranted(): Boolean {
    for (permission in requiredPermissions) {
      if (!isPermissionGranted(this, permission)) {
        return false
      }
    }
    return true
  }

  private val runtimePermissions: Unit
    get() {
      val allNeededPermissions: MutableList<String?> = ArrayList()
      for (permission in requiredPermissions) {
        if (!isPermissionGranted(this, permission)) {
          allNeededPermissions.add(permission)
        }
      }
      if (allNeededPermissions.isNotEmpty()) {
        ActivityCompat.requestPermissions(
          this,
          allNeededPermissions.toTypedArray(),
          PERMISSION_REQUESTS
        )
      }
    }

  @SuppressLint("UnsafeOptInUsageError")
  override fun onRequestPermissionsResult(
    requestCode: Int,
    permissions: Array<String>,
    grantResults: IntArray
  ) {
    Log.i(TAG, "Permission granted!")
    if (allPermissionsGranted()) {
      bindAllCameraUseCases()
    }
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
  }

  companion object {

    const val EXTRA_INVENTORY_ID = "EXTRA_INVENTORY_ID"
    const val EXTRA_ZONE_ID = "EXTRA_ZONE_ID"

    private const val TAG = "CameraXLivePreview"
    private const val PERMISSION_REQUESTS = 1

    private fun isPermissionGranted(context: Context, permission: String?): Boolean {
      if (ContextCompat.checkSelfPermission(context, permission!!) ==
          PackageManager.PERMISSION_GRANTED
      ) {
        Log.i(TAG, "Permission granted: $permission")
        return true
      }
      Log.i(TAG, "Permission NOT granted: $permission")
      return false
    }
  }

  private fun askToAddProduct(barcodeValue: String, otherBarcodes: List<Barcode>) {
    AlertDialog.Builder(this)
      .setTitle(R.string.add_product_title)
      .setMessage(getString(R.string.add_barcode_question, barcodeValue))
      .setPositiveButton(android.R.string.yes) { _, _ -> addProductWithBarcode(barcodeValue) }
      .setNegativeButton(android.R.string.no) { _, _ ->   parseBarcodes(otherBarcodes)}
      .show()
  }

  private fun parseBarcodes(barcodes: List<Barcode>) {
    if (barcodes.isEmpty()) {
      imageProcessor?.resume()
      return
    }
    val barcode = barcodes[0]
    val otherBarcodes = if (barcodes.size > 1)  barcodes.subList(1, barcodes.size) else emptyList()
    barcode.rawValue?.let {
      askToAddProduct(it, otherBarcodes)
    } ?: run {
      parseBarcodes(otherBarcodes)
    }
  }

  private fun addProductWithBarcode(rawValue: String) {
    addProductsViewModel.addProduct(apiClient.apiService, inventoryId, zoneId, rawValue)
  }


  override fun onBarcodesScanned(barcodes: List<Barcode>) {
    imageProcessor?.pause()
    parseBarcodes(barcodes)
  }

  private fun displayAddProductDialogFragment() {
    val fm: FragmentManager = supportFragmentManager
    val editNameDialogFragment: AddProductDialogFragment = AddProductDialogFragment.newInstance(zoneId, inventoryId)
    editNameDialogFragment.show(fm, "fragment_add_product")
  }
}
