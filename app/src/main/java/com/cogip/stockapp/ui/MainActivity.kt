package com.cogip.stockapp.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.cogip.stockapp.R
import com.cogip.stockapp.data.ApiClient
import com.cogip.stockapp.utils.SessionManager

class MainActivity : AppCompatActivity() {
    private lateinit var apiClient: ApiClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        apiClient = ApiClient(application)


        findViewById<Button>(R.id.authenticate_MainAct_Button).setOnClickListener {
            fetchAuthToken()
        }

        findViewById<Button>(R.id.searchInventories_MainAct_Button).setOnClickListener {
            if (SessionManager(this.application).fetchAuthToken().isNullOrEmpty()) {
                val toast = Toast.makeText(this@MainActivity, "Sorry, you must authenticate before", Toast.LENGTH_LONG)
                toast.show()
            } else {
                displayInventoriesScreen()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (SessionManager(this.application).fetchAuthToken().isNullOrEmpty()) {
            fetchAuthToken()
        } else {
            displayInventoriesScreen()
        }
    }



    private fun displayInventoriesScreen() {
        //if (sessionManager.fetchAuthToken().isNullOrEmpty()) {
        val intent = Intent(this, InventoriesActivity::class.java)
        startActivity(intent)
    }

    private fun fetchAuthToken() {
        //if (sessionManager.fetchAuthToken().isNullOrEmpty()) {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }
}
