/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cogip.stockapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cogip.stockapp.R
import com.cogip.stockapp.barcodescanner.CameraXLivePreviewPreferenceFragment
import java.lang.Exception
import java.lang.RuntimeException

class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        val actionBar = supportActionBar
        actionBar?.setTitle(R.string.pref_category_title_camera)
        try {
            fragmentManager
                .beginTransaction()
                .replace(
                    R.id.settings_container,
                    CameraXLivePreviewPreferenceFragment::class.java.getDeclaredConstructor()
                        .newInstance()
                )
                .commit()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}