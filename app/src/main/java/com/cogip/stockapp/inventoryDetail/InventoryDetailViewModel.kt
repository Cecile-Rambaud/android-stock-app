package com.cogip.stockapp.inventoryDetail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cogip.stockapp.data.ApiService
import com.cogip.stockapp.data.models.Inventory
import com.cogip.stockapp.data.models.Zone
import kotlinx.coroutines.*

class InventoryDetailViewModel : ViewModel() {

    private var job: Job? = null

    private val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }
    val inventory = MutableLiveData<Inventory>()
    val zones = MutableLiveData<List<Zone>>()
    val inventoryLoadError = MutableLiveData<String?>()
    val loading = MutableLiveData<Boolean>()

    fun refresh(apiService: ApiService, inventoryId: Int) {
        fetchInventory(apiService, inventoryId)
        fetchZones(apiService)
    }

    @DelicateCoroutinesApi
    private fun fetchInventory(apiService: ApiService, inventoryId: Int) {
        loading.value = true
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = apiService.getInventory(inventoryId)
            withContext(Dispatchers.Main) {
                val responseValue = response.body()
                if (responseValue != null && response.isSuccessful) {
                    inventory.value = Inventory(responseValue)
                    inventoryLoadError.value = null
                    loading.value = false
                } else if(response.isSuccessful){
                    onError("Error : inventory not found")
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
        inventoryLoadError.value = ""
        loading.value = false
    }

    private fun fetchZones(apiService: ApiService) {
        loading.value = true
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = apiService.getZones(1)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    zones.value = response.body()
                    inventoryLoadError.value = null
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
        inventoryLoadError.value = ""
        loading.value = false
    }

    @DelicateCoroutinesApi
    private fun onError(message: String) {
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                inventoryLoadError.value = message
                loading.value = false
            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}