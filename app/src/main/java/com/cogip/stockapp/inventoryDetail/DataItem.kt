package com.cogip.stockapp.inventoryDetail

import com.cogip.stockapp.data.models.Zone

sealed class DataItem {

    abstract val id: Int

    data class ZoneItem(val zone: Zone): DataItem()      {
        override val id = zone.id
    }

    object Header: DataItem() {
        override val id = Int.MIN_VALUE
    }

}
