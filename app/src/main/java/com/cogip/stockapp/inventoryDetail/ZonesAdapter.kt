package com.cogip.stockapp.inventoryDetail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cogip.stockapp.R
import com.cogip.stockapp.data.models.Zone
private const val ITEM_VIEW_TYPE_HEADER = 0
private const val ITEM_VIEW_TYPE_ITEM = 1

class ZonesAdapter(private val onClick: (Zone) -> Unit, private var items: ArrayList<DataItem> = arrayListOf()) :
    ListAdapter<DataItem, RecyclerView.ViewHolder>(ZoneDiffCallback) {


    /* Creates and inflates view and return ProductViewHolder. */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ITEM_VIEW_TYPE_HEADER)
            TextViewHolder.from(parent)
        else
            ZoneViewHolder.from(parent, onClick)
    }

    override fun getItemCount() = items.size

    override fun getItem(position: Int): DataItem {
        return items[position]
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is DataItem.Header -> ITEM_VIEW_TYPE_HEADER
            is DataItem.ZoneItem -> ITEM_VIEW_TYPE_ITEM
        }
    }
    /* Gets current Product and uses it to bind view. */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val dataItem = getItem(position)
        if ((holder is ZoneViewHolder) && (dataItem is DataItem.ZoneItem)) {
            holder.bind(dataItem.zone)
        }
    }

    /* ViewHolder for Zone, takes in the inflated view and the onClick behavior. */
    class ZoneViewHolder(itemView: View, val onClick: (Zone) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        private val zoneNameTextView: TextView = itemView.findViewById(R.id.zoneItemNameTv)
        private var currentZone: Zone? = null

        companion object {
            fun from(parent: ViewGroup, onClick: (Zone) -> Unit): RecyclerView.ViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.zone_item, parent, false)
                return ZoneViewHolder(view, onClick)
            }
        }

        init {
            itemView.setOnClickListener {
                currentZone?.let {
                    onClick(it)
                }
            }
        }

        /* Bind Product name and image. */
        fun bind(zone: Zone) {
            currentZone = zone
            zoneNameTextView.text = zone.name
        }
    }

    fun updateForDataChanges(_zones: List<Zone>) {
        items.clear()
        items.add(DataItem.Header)
        val zoneItems = ArrayList<DataItem.ZoneItem>()
        _zones.forEach {
            zoneItems.add(DataItem.ZoneItem(it))
        }
        items.addAll(
            zoneItems
        )
        notifyDataSetChanged()
    }

    class TextViewHolder(view: View): RecyclerView.ViewHolder(view) {
        companion object {
            fun from(parent: ViewGroup): TextViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.zone_header, parent, false)
                return TextViewHolder(view)
            }
        }
    }
}

object ZoneDiffCallback : DiffUtil.ItemCallback<DataItem>() {
    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.id == newItem.id
    }
}