package com.cogip.stockapp.inventoryList

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cogip.stockapp.R
import com.cogip.stockapp.data.models.Inventory

private const val ITEM_VIEW_TYPE_HEADER = 0
private const val ITEM_VIEW_TYPE_ITEM = 1

class InventoriesAdapter(
    private val clickListener: InventoryListener,
    private var items: ArrayList<DataItem> = arrayListOf()
) : ListAdapter<DataItem, RecyclerView.ViewHolder>(InventoryDiffCallback) {


    class InventoryListener(val clickListener: (inventoryId: Int) -> Unit) {
        fun onClick(inventory: Inventory) = clickListener(inventory.id)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ITEM_VIEW_TYPE_HEADER)
            TextViewHolder.from(parent)
        else
            InventoryViewHolder.from(parent, clickListener)
    }

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is DataItem.Header -> ITEM_VIEW_TYPE_HEADER
            is DataItem.InventoryItem -> ITEM_VIEW_TYPE_ITEM
        }
    }

    override fun getItem(position: Int): DataItem {
        return items[position]
    }

    /* Gets current Inventory and uses it to bind view. */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val dataItem = getItem(position)
        if ((holder is InventoryViewHolder) && (dataItem is DataItem.InventoryItem)) {
            holder.bind(dataItem.inventory)
        }
    }

    fun updateForDataChanges(_inventories: List<Inventory>) {
        items.clear()
        items.add(DataItem.Header)
        val inventoryItems = ArrayList<DataItem.InventoryItem>()
        _inventories.forEach {
            inventoryItems.add(DataItem.InventoryItem(it))
        }
        items.addAll(
            inventoryItems
        )
        notifyDataSetChanged()
    }


    /* ViewHolder for Inventory, takes in the inflated view and the onClick behavior. */
    class InventoryViewHolder(itemView: View, clickListener: InventoryListener) :
        RecyclerView.ViewHolder(itemView) {
        private val idTextView: TextView = itemView.findViewById(R.id.inventory_item_id_tv)
        private val startDateTextView: TextView = itemView.findViewById(R.id.inventory_item_start_date_tv)
        private val endDateTextView: TextView = itemView.findViewById(R.id.inventory_item_end_date_tv)
        private val stateTextView: TextView = itemView.findViewById(R.id.inventory_item_state_tv)
        private var currentInventory: Inventory? = null

        companion object {
            fun from(parent: ViewGroup, clickListener: InventoryListener): RecyclerView.ViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.inventory_item, parent, false)
                return InventoryViewHolder(view, clickListener)
            }
        }

        init {
            itemView.setOnClickListener {
                currentInventory?.let {
                    clickListener.onClick(it)
                }
            }
        }

        fun bind(inventory: Inventory) {
            currentInventory = inventory
            idTextView.text = "" + inventory.id
            startDateTextView.text = "" + inventory.startDate //TODO date comprehensible
            endDateTextView.text = "" + inventory.endDate //TODO date comprehensible
            stateTextView.text = "" + inventory.state //TODO : traduire en valeur
            itemView.setBackgroundColor(Color.WHITE)
        }
    }

    class TextViewHolder(view: View): RecyclerView.ViewHolder(view) {
        companion object {
            fun from(parent: ViewGroup): TextViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.inventory_header, parent, false)
                return TextViewHolder(view)
            }
        }
    }
}

object InventoryDiffCallback : DiffUtil.ItemCallback<DataItem>() {
    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.id == newItem.id
    }
}