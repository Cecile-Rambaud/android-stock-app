package com.cogip.stockapp.inventoryList

import com.cogip.stockapp.data.models.Inventory

sealed class DataItem {

    abstract val id: Int

    data class InventoryItem(val inventory: Inventory): DataItem()      {
        override val id = inventory.id
    }

    object Header: DataItem() {
        override val id = Int.MIN_VALUE
    }

}
