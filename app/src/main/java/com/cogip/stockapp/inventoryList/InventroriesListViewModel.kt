package com.cogip.stockapp.inventoryList

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cogip.stockapp.data.ApiService
import com.cogip.stockapp.data.models.Inventory
import kotlinx.coroutines.*

class InventoriesListViewModel : ViewModel() {

    private var job: Job? = null
    private val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }
    val inventories = MutableLiveData<List<Inventory>>()
    val inventoriesLoadError = MutableLiveData<String?>()
    val loading = MutableLiveData<Boolean>()

    fun refresh(apiService: ApiService) {
        fetchProducts(apiService)
    }

    private fun fetchProducts(apiService: ApiService) {
        loading.value = true
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = apiService.getInventories()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    inventories.value = response.body()
                    inventoriesLoadError.value = null
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
        inventoriesLoadError.value = ""
        loading.value = false
    }

    private fun onError(message: String) {
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                inventoriesLoadError.value = message
                loading.value = false
            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}