package com.cogip.stockapp.barcodescanner

import android.content.Context
import android.util.Log
import com.cogip.stockapp.utils.Constants
import com.google.android.gms.tasks.Task
import com.google.mlkit.vision.barcode.BarcodeScanner
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.barcode.common.Barcode
import com.google.mlkit.vision.common.InputImage

/** Barcode Detector Demo.  */
class BarcodeScannerProcessor(
    context: Context,
    private val successListener: BarcodeScannerSuccessListener
) : VisionProcessorBase<List<Barcode>>(context) {

    // Note that if you know which format of barcode your app is dealing with, detection will be
    // faster to specify the supported barcode formats one by one, e.g.
    // BarcodeScannerOptions.Builder()
    //     .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
    //     .build();
    private val barcodeScanner: BarcodeScanner = BarcodeScanning.getClient()
    private var paused = false

    override fun stop() {
        super.stop()
        barcodeScanner.close()
    }

    override fun detectInImage(image: InputImage): Task<List<Barcode>> {
        return barcodeScanner.process(image)
    }

    override fun onSuccess(barcodes: List<Barcode>, graphicOverlay: GraphicOverlay) {
        if (paused) return
        if (barcodes.isEmpty()) {
            Log.v(Constants.APP_LOG_TAG, "No barcode has been detected")
        }
        successListener.onBarcodesScanned(barcodes)
    }

    override fun onFailure(e: Exception) {
        Log.e(TAG, "Barcode detection failed $e")
    }

    fun pause() {
        paused = true
    }

    fun resume() {
        paused = false
    }

    companion object {
        private const val TAG = "BarcodeProcessor"
    }

    interface BarcodeScannerSuccessListener {
        fun onBarcodesScanned(barcodes: List<Barcode>)
    }

}

