package com.cogip.stockapp.utils

import com.cogip.stockapp.data.models.DateTime
import java.text.SimpleDateFormat
import java.util.*

class DateUtils {
    companion object {
        fun dateTimeToString(dateTime: DateTime?): String {
            if (dateTime == null) {
                return ""
            }
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US)
            return sdf.format(dateTime)
        }
    }
}