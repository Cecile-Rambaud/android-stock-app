/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cogip.stockapp.utils

import android.content.Context
import androidx.annotation.StringRes
import android.preference.PreferenceManager
import com.cogip.stockapp.barcodescanner.CameraSource.SizePair
import com.cogip.stockapp.barcodescanner.CameraSource
import com.cogip.stockapp.R
import androidx.annotation.RequiresApi
import android.os.Build.VERSION_CODES
import android.util.Size
import androidx.camera.core.CameraSelector
import java.lang.Exception
import com.google.common.base.Preconditions


/** Utility class to retrieve shared preferences.  */
class PreferenceUtils {
    companion object {
        private const val POSE_DETECTOR_PERFORMANCE_MODE_FAST = 1


        @JvmStatic
        fun saveString(context: Context, @StringRes prefKeyId: Int, value: String) {
            PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(context.getString(prefKeyId), value)
                .apply()
        }

        fun getCameraPreviewSizePair(context: Context, cameraId: Int): SizePair? {
            // Preconditions.checkArgument(cameraId == CameraSource.CAMERA_FACING_BACK || cameraId == CameraSource.CAMERA_FACING_FRONT);
            val previewSizePrefKey: String
            val pictureSizePrefKey: String
            if (cameraId == CameraSource.CAMERA_FACING_BACK) {
                previewSizePrefKey = context.getString(R.string.pref_key_rear_camera_preview_size)
                pictureSizePrefKey = context.getString(R.string.pref_key_rear_camera_picture_size)
            } else {
                previewSizePrefKey = context.getString(R.string.pref_key_front_camera_preview_size)
                pictureSizePrefKey = context.getString(R.string.pref_key_front_camera_picture_size)
            }
            return try {
                val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
                SizePair(
                    com.google.android.gms.common.images.Size.parseSize(
                        sharedPreferences.getString(
                            previewSizePrefKey,
                            null
                        )!!
                    ),
                    com.google.android.gms.common.images.Size.parseSize(
                        sharedPreferences.getString(
                            pictureSizePrefKey,
                            null
                        )!!
                    )
                )
            } catch (e: Exception) {
                null
            }
        }

        @RequiresApi(VERSION_CODES.LOLLIPOP)
        fun getCameraXTargetResolution(context: Context, lensfacing: Int): Size? {
            Preconditions.checkArgument(lensfacing == CameraSelector.LENS_FACING_BACK || lensfacing == CameraSelector.LENS_FACING_FRONT)
            val prefKey =
                if (lensfacing == CameraSelector.LENS_FACING_BACK) context.getString(R.string.pref_key_camerax_rear_camera_target_resolution) else context.getString(
                    R.string.pref_key_camerax_front_camera_target_resolution
                )
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            return try {
                Size.parseSize(sharedPreferences.getString(prefKey, null))
            } catch (e: Exception) {
                null
            }
        }

        fun shouldHideDetectionInfo(context: Context): Boolean {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val prefKey = context.getString(R.string.pref_key_info_hide)
            return sharedPreferences.getBoolean(prefKey, false)
        }

        /**
         * Mode type preference is backed by [android.preference.ListPreference] which only support
         * storing its entry value as string type, so we need to retrieve as string and then convert to
         * integer.
         */
        private fun getModeTypePreferenceValue(
            context: Context, @StringRes prefKeyResId: Int, defaultValue: Int
        ): Int {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val prefKey = context.getString(prefKeyResId)
            return sharedPreferences.getString(prefKey, defaultValue.toString())!!.toInt()
        }

        fun isCameraLiveViewportEnabled(context: Context): Boolean {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val prefKey = context.getString(R.string.pref_key_camera_live_viewport)
            return sharedPreferences.getBoolean(prefKey, false)
        }

    }
}