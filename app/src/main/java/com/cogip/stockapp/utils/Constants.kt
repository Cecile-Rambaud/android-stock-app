package com.cogip.stockapp.utils

object Constants {

    // Endpoints
    const val APP_LOG_TAG = "STOCK_APP"
    //const val BASE_URL = "http://192.168.1.70/"
    const val BASE_URL = "https://agross.eaf-slam-22.fr.nf/"
    //const val BASE_URL = "http://127.0.0.1/"
    const val LOGIN_URL = "authentication_token"
    const val PRODUCTS_URL = "api/products"
    const val ZONES_URL = "api/zones"
    const val INVENTORIES_URL = "api/inventories"
    const val MEDIAOBJECT_URL = "api/media_objects"
    const val INVENTORY_PRODUCTS_URL = "api/inventory_products"

    const val INTENT_KEY_ERROR = "KEY_ERROR"


}