package com.cogip.stockapp.utils

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.cogip.stockapp.R

/**
 * Session manager to save and fetch data from SharedPreferences
 */
class SessionManager (context: Application) {
    private var prefs: SharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE)

    companion object {
        const val USER_TOKEN = "user_token"
        const val REFRESH_TOKEN = "refresh_token"
    }

    fun removeTokens() {
        val editor = prefs.edit()
        editor.remove(USER_TOKEN)
        editor.remove(REFRESH_TOKEN)
        editor.apply()
    }

    /**
     * Function to save auth and refresh tokens
     */
    fun saveAccessTokens(authToken: String, refreshToken: String) {
        saveAuthToken(authToken)
        saveRefreshToken(refreshToken)
    }

    /**
     * Function to save auth token
     */
    private fun saveAuthToken(token: String) {
        val editor = prefs.edit()
        editor.putString(USER_TOKEN, token)
        editor.apply()
    }

    /**
     * Function to fetch auth token
     */
    fun fetchAuthToken(): String? {
        return prefs.getString(USER_TOKEN, null)
    }

    /**
     * Function to save refresh token
     */
    private fun saveRefreshToken(token: String) {
        val editor = prefs.edit()
        editor.putString(REFRESH_TOKEN, token)
        editor.apply()
    }

    /**
     * Function to fetch auth token
     */
    fun fetchRefreshToken(): String? {
        return prefs.getString(REFRESH_TOKEN, null)
    }
}