package com.cogip.stockapp.productList

import com.cogip.stockapp.data.models.Product

sealed class DataItem {

    abstract val id: Int

    data class ProductItem(val product: Product, val qty: Int): DataItem()      {
        override val id = product.id
    }

    object Header: DataItem() {
        override val id = Int.MIN_VALUE
    }

}
