package com.cogip.stockapp.productList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cogip.stockapp.R
import com.cogip.stockapp.data.models.InventoryProducts
import com.cogip.stockapp.data.models.Product

private const val ITEM_VIEW_TYPE_HEADER = 0
private const val ITEM_VIEW_TYPE_ITEM = 1

class ProductsAdapter(private val onClick: (Product) -> Unit, private var items: ArrayList<DataItem> = arrayListOf()) :
    ListAdapter<DataItem, RecyclerView.ViewHolder>(ProductDiffCallback) {


    /* Creates and inflates view and return ProductViewHolder. */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ITEM_VIEW_TYPE_HEADER)
            TextViewHolder.from(parent)
        else
            ProductViewHolder.from(parent, onClick)
    }

    override fun getItemCount() = items.size

    override fun getItem(position: Int): DataItem {
        return items[position]
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is DataItem.Header -> ITEM_VIEW_TYPE_HEADER
            is DataItem.ProductItem -> ITEM_VIEW_TYPE_ITEM
        }
    }
    /* Gets current Product and uses it to bind view. */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val dataItem = getItem(position)
        if ((holder is ProductViewHolder) && (dataItem is DataItem.ProductItem)) {
            holder.bind(dataItem.product, dataItem.qty)
        }
    }

    /* ViewHolder for Product, takes in the inflated view and the onClick behavior. */
    class ProductViewHolder(itemView: View, val onClick: (Product) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        private val productQtyTextView: TextView = itemView.findViewById(R.id.productItemQtyTV)
        private val productBarcodeTextView: TextView = itemView.findViewById(R.id.productItemBarcodeTV)
        private val productNameTextView: TextView = itemView.findViewById(R.id.productItemNameTV)
        private val productIdTextView: TextView = itemView.findViewById(R.id.productItemIdTV)
        private val productImageView: ImageView = itemView.findViewById(R.id.productItemImageIV)
        private var currentProduct: Product? = null

        companion object {
            fun from(parent: ViewGroup, onClick: (Product) -> Unit): RecyclerView.ViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.product_item, parent, false)
                return ProductViewHolder(view, onClick)
            }
        }

        init {
            itemView.setOnClickListener {
                currentProduct?.let {
                    onClick(it)
                }
            }
        }

        /* Bind Product name and image. */
        fun bind(product: Product, qty: Int) {
            currentProduct = product
            productIdTextView.text = "${product.id}"
            productNameTextView.text = product.name
            productBarcodeTextView.text = product.barcode
            productQtyTextView.text = "$qty"
            //if (product.image != null) {
            //    productImageView.setImageResource(product.image)
            //} else {
            productImageView.setImageResource(R.drawable.outline_broken_image_black_24)
            //}
        }
    }

    fun updateForDataChanges(_inventoryProducts: List<InventoryProducts>) {
        items.clear()
        items.add(DataItem.Header)
        val productItems = ArrayList<DataItem.ProductItem>()
        _inventoryProducts.forEach {
            productItems.add(DataItem.ProductItem(it.product, it.qty))
        }
        items.addAll(
            productItems
        )
        notifyDataSetChanged()
    }

    class TextViewHolder(view: View): RecyclerView.ViewHolder(view) {
        companion object {
            fun from(parent: ViewGroup): TextViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.product_header, parent, false)
                return TextViewHolder(view)
            }
        }
    }
}

object ProductDiffCallback : DiffUtil.ItemCallback<DataItem>() {
    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.id == newItem.id
    }
}