package com.cogip.stockapp.productList

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cogip.stockapp.data.ApiService
import com.cogip.stockapp.data.models.InventoryProducts
import com.cogip.stockapp.data.requests.InventoryProductRequest
import com.cogip.stockapp.utils.Constants
import kotlinx.coroutines.*

class InventoryAddProductsViewModel : ViewModel() {

    private var job: Job? = null

    val inventoryProductAdded = MutableLiveData<InventoryProducts>()
    val productsAddError = MutableLiveData<String?>()
    val addingProduct = MutableLiveData<Boolean>()
    private val exceptionHandlerAdd = CoroutineExceptionHandler { coroutineContext, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }


    fun addProduct(apiService: ApiService, inventoryId: Int, zoneId: Int, barcode: String) {
        addingProduct.value = true
        job = CoroutineScope(Dispatchers.IO + exceptionHandlerAdd).launch {
            val getProductsResponse = apiService.getProducts(1, barcode)
            if (getProductsResponse.isSuccessful) {
                    val products  = getProductsResponse.body()
                    if (products == null || products.isEmpty()) {
                        onError("Error : no product found for barcode $barcode ")
                    } else {
                        val getInventoryProductResponse = apiService.getInventoryProduct(products[0].id, inventoryId)
                        if (getInventoryProductResponse.isSuccessful) {
                            val inventoryProducts  = getInventoryProductResponse.body()
                            if (inventoryProducts == null) {
                                onError("Error retrieving product ${products[0].id} in inventory $inventoryId ")
                            } else {
                                if (inventoryProducts.isNotEmpty()) {
                                    val inventoryProduct = inventoryProducts[0]
                                    if (inventoryProduct.zone.id != zoneId) {
                                        onError("Sorry but th product ${products[0].barcode} is registered in zone ${inventoryProducts[0].zone.id} ")
                                    } else {
                                        val qty = inventoryProduct.qty + 1
                                        val data = InventoryProductRequest(
                                            "/${Constants.INVENTORIES_URL}/${inventoryId}",
                                            "/${Constants.ZONES_URL}/${zoneId}",
                                            "/${Constants.PRODUCTS_URL}/${products[0].id}",
                                            qty
                                        )
                                        val response = apiService.updateInventoryProduct(inventoryProduct.id, data)
                                        withContext(Dispatchers.Main) {
                                            if (response.isSuccessful) {
                                                inventoryProductAdded.value = response.body()
                                                addingProduct.value = false
                                            } else {
                                                onError(
                                                    "Error : ${response.message()} "
                                                )
                                            }
                                        }
                                    }

                                } else {
                                    val data = InventoryProductRequest(
                                        "/${Constants.INVENTORIES_URL}/${inventoryId}",
                                        "/${Constants.ZONES_URL}/${zoneId}",
                                        "/${Constants.PRODUCTS_URL}/${products[0].id}",
                                        1
                                    )
                                    val response = apiService.addInventoryProduct(data)
                                    withContext(Dispatchers.Main) {
                                        if (response.isSuccessful) {
                                            inventoryProductAdded.value = response.body()
                                            addingProduct.value = false
                                        } else {
                                            onError(
                                                "Error : ${response.message()} "
                                            )
                                        }
                                    }
                                }
                            }
                        } else {
                            onError("Error : ${getInventoryProductResponse.message()} ")
                        }

                    }
                } else {
                    onError("Error : ${getProductsResponse.message()} ")
                }

        }
        productsAddError.value = ""
    }



    private fun onError(message: String) {
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                productsAddError.value = message
                addingProduct.value = false
            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}