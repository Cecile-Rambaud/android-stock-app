package com.cogip.stockapp.productList

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cogip.stockapp.data.ApiService
import com.cogip.stockapp.data.models.InventoryProducts
import kotlinx.coroutines.*

class InventoryProductsListViewModel : ViewModel() {

    private var job: Job? = null

    val inventoryProducts = MutableLiveData<List<InventoryProducts>>()
    val productsLoadError = MutableLiveData<String?>()
    val loading = MutableLiveData<Boolean>()
    private val exceptionHandlerLoad= CoroutineExceptionHandler { coroutineContext, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }

    fun refresh(apiService: ApiService, inventoryId : Int, zoneId: Int) {
        fetchInventoryProducts(apiService, inventoryId, zoneId)
    }

    private fun fetchInventoryProducts(apiService: ApiService, inventoryId: Int, zoneId: Int) {
        loading.value = true
        job = CoroutineScope(Dispatchers.IO + exceptionHandlerLoad).launch {
            val response = apiService.getInventoryProducts(1, inventoryId, zoneId)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    inventoryProducts.value = response.body()
                    productsLoadError.value = null
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
        productsLoadError.value = ""
        loading.value = false
    }

    private fun onError(message: String) {
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                productsLoadError.value = message
                loading.value = false
            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}