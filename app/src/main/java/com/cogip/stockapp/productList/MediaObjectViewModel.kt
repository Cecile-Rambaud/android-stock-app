package com.cogip.stockapp.productList

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cogip.stockapp.data.ApiService
import kotlinx.coroutines.*

class MediaObjectViewModel : ViewModel() {

    private var job: Job? = null

    val contentUrl = MutableLiveData<String?>()
    val mediaObjectLoadError = MutableLiveData<String?>()
    val loading = MutableLiveData<Boolean>()
    private val exceptionHandlerLoad= CoroutineExceptionHandler { coroutineContext, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }


    private fun fetchMediaObject(apiService: ApiService, mediaObjectId: Int) {
        loading.value = true
        job = CoroutineScope(Dispatchers.IO + exceptionHandlerLoad).launch {
            val response = apiService.getMediaObject(mediaObjectId)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    contentUrl.value = response.body()?.contentUrl
                    mediaObjectLoadError.value = null
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
        mediaObjectLoadError.value = ""
        loading.value = false
    }

    private fun onError(message: String) {
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                mediaObjectLoadError.value = message
                loading.value = false
            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}