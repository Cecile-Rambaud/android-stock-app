package com.cogip.stockapp.data

import android.app.Application
import android.util.Log
import com.cogip.stockapp.data.models.DateTime
import com.cogip.stockapp.utils.Constants
import com.cogip.stockapp.utils.SessionManager
import com.google.gson.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Retrofit instance class
 */
class ApiClient(context: Application)
{
    val apiService: ApiService


    init {
//        val gson = GsonBuilder()
//            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
//            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gsonWithDate()))
            .client(okhttpClient(context))
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    private fun gsonWithDate(): Gson {
        val builder = GsonBuilder()
        builder.registerTypeAdapter(DateTime::class.java, object : JsonDeserializer<DateTime?> {
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US)

            @Throws(JsonParseException::class)
            override fun deserialize(
                json: JsonElement,
                typeOfT: Type?,
                context: JsonDeserializationContext?
            ): DateTime? {
                return try {
                   if (json.asString == null)  null
                   else {
                       val str = (json.asString)
                       val aDate = df.parse(str as String)
                       if (aDate == null)  null
                       else DateTime(aDate)
                   }
                } catch (e: ParseException) {
                    Log.e(Constants.APP_LOG_TAG, e.message, e)
                    null
                }
            }
        })
        return builder.create()
    }
    /**
     * Initialize OkhttpClient with our interceptor
     */
    private fun okhttpClient(context: Application): OkHttpClient {
        val sessionManager = SessionManager(context)
        return OkHttpClient.Builder()
            //.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .addInterceptor(AuthInterceptor(sessionManager, context))
            .retryOnConnectionFailure(true)
            .authenticator(AccessTokenAuthenticator(sessionManager, context))
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .build()
    }
}