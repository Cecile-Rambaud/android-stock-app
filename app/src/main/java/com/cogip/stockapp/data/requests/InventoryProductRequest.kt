package com.cogip.stockapp.data.requests

import com.google.gson.annotations.SerializedName

data class InventoryProductRequest (

    @SerializedName("inventory")
    var inventory: String,

    @SerializedName("zone")
    var zone: String,

    @SerializedName("product")
    var product: String,

    @SerializedName("qty")
    var qty: Int
)