package com.cogip.stockapp.data.models

import com.google.gson.annotations.SerializedName

data class Zone (
    @SerializedName("id")
    var id: Int,

    @SerializedName("name")
    var name: String,
)