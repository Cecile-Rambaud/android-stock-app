package com.cogip.stockapp.data.models

import com.google.gson.annotations.SerializedName

data class InventoryProducts (

    @SerializedName("id")
    var id: Int,

    @SerializedName("product")
    var product: Product,

    @SerializedName("inventory")
    var inventory: Inventory,

    @SerializedName("zone")
    var zone: Zone,

    @SerializedName("qty")
    var qty: Int

) 