package com.cogip.stockapp.data

import android.content.Context
import android.util.Log
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.cogip.stockapp.utils.Constants
import com.cogip.stockapp.utils.SessionManager
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

class AccessTokenAuthenticator(sessionManager: SessionManager, _context: Context) : Authenticator {

    private var accessTokenRepository: SessionManager = sessionManager
    private var context: Context

    init {
        this.context = _context
    }
    
    @Nullable
    override fun authenticate(route: Route?, response: Response): Request? {
        if (!isRequestWithAccessToken(response)) {
            return null
        }

        if (!response.isSuccessful && response.code == 401) {
            accessTokenRepository.removeTokens()
            Log.d(Constants.APP_LOG_TAG, "Problème d'authentification : " + response.message)
            return null
        }

        val accessToken: String = accessTokenRepository.fetchAuthToken() ?: return null

        synchronized(this) {
            val newAccessToken: String?= accessTokenRepository.fetchAuthToken()
            // Access token is refreshed in another thread.
            newAccessToken?.let{
                if (accessToken != newAccessToken) {
                    return newRequestWithAccessToken(response.request, it)
                }
            }

            // Need to refresh an access token
            val updatedAccessToken: String = accessTokenRepository.fetchRefreshToken() ?: return null
            return newRequestWithAccessToken(response.request, updatedAccessToken)
        }
    }

    private fun isRequestWithAccessToken(@NonNull response: Response): Boolean {
        val header: String? = response.request.header("Authorization")
        return header != null && header.startsWith("Bearer")
    }

    @NonNull
    private fun newRequestWithAccessToken(
        @NonNull request: Request,
        @NonNull accessToken: String
    ): Request {
        return request.newBuilder()
            .header("Authorization", "Bearer $accessToken")
            .build()
    }
}