package com.cogip.stockapp.data.models

import com.google.gson.annotations.SerializedName

data class Product (
    @SerializedName("id")
    var id: Int,

    @SerializedName("name")
    var name: String,

    @SerializedName("barcode")
    var barcode: String,

    @SerializedName("price")
    var price: Double,

    @SerializedName("availability")
    var availability: Int
)