package com.cogip.stockapp.data

import com.cogip.stockapp.data.models.*
import com.cogip.stockapp.data.requests.InventoryProductRequest
import com.cogip.stockapp.data.requests.LoginRequest
import com.cogip.stockapp.data.responses.*
import com.cogip.stockapp.utils.Constants
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

/**
 * Interface for defining REST request functions
 */
interface ApiService {

    @Headers("Content-Type: application/json")
    @POST(Constants.LOGIN_URL)
    fun login(@Body request: LoginRequest): Call<LoginResponse>

    @GET(Constants.ZONES_URL)
    suspend fun getZones(@Query("page") page: Int): Response<List<Zone>>


    @GET(Constants.PRODUCTS_URL)
    suspend fun getProducts(@Query("page") page: Int,@Query("barcode") barcode: String): Response<List<Product>>

    @GET(Constants.INVENTORIES_URL)
    suspend fun getInventories(): Response<List<Inventory>>

    @GET(Constants.INVENTORIES_URL+"/{id}")
    suspend fun getInventory(@Path("id") inventoryId: Int): Response<InventoryResponse>

    @GET(Constants.MEDIAOBJECT_URL+"/{id}")
    suspend fun getMediaObject(@Path("id") mediaObjectId: Int): Response<MediaObject>

    @GET(Constants.INVENTORY_PRODUCTS_URL)
    suspend fun getInventoryProducts(@Query("page") page: Int, @Query("inventory.id") inventoryId: Int, @Query("zone.id") zoneId: Int): Response<List<InventoryProducts>>

    @GET(Constants.INVENTORY_PRODUCTS_URL)
    suspend fun getInventoryProduct(@Query("product.id") productId: Int, @Query("inventory.id") inventoryId: Int): Response<List<InventoryProducts>>


    @POST(Constants.INVENTORY_PRODUCTS_URL)
    suspend fun addInventoryProduct(@Body request: InventoryProductRequest): Response<InventoryProducts>

    @PUT(Constants.INVENTORY_PRODUCTS_URL+"/{id}")
    suspend fun updateInventoryProduct(@Path("id") inventoryProductId: Int, @Body request: InventoryProductRequest): Response<InventoryProducts>
}