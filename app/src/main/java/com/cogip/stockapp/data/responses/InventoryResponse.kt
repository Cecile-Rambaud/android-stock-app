package com.cogip.stockapp.data.responses

import com.cogip.stockapp.data.models.DateTime
import com.google.gson.annotations.SerializedName

data class InventoryResponse (

    @SerializedName("id")
    var id: Int,

    @SerializedName("startDate")
    var startDate: DateTime?,

    @SerializedName("endDate")
    var endDate: DateTime?,

    @SerializedName("state")
    var state: Int,

    @SerializedName("timeCreated")
    var timeCreated: DateTime,

    @SerializedName("timeModified")
    var timeModified: DateTime?,
)