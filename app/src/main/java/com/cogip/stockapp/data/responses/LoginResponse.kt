package com.cogip.stockapp.data.responses

import com.google.gson.annotations.SerializedName

data class LoginResponse (
    @SerializedName("token")
    var authToken: String,

    @SerializedName("refresh_token")
    var refreshToken: String
)