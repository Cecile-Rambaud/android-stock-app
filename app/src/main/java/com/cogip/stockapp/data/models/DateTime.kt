package com.cogip.stockapp.data.models

import java.util.*

class DateTime : Date {
    constructor(readLong: Long) : super(readLong)
    constructor(date: Date) : super(date.time)
}