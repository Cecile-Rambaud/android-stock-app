package com.cogip.stockapp.data.responses

import com.cogip.stockapp.data.models.DateTime
import com.cogip.stockapp.data.models.Product
import com.google.gson.annotations.SerializedName

data class ZoneResponse (

    @SerializedName("id")
    var id: Int,

    @SerializedName("name")
    var name: String,

    @SerializedName("products")
    var products: List<Product>,

    @SerializedName("timeCreated")
    var timeCreated: DateTime,

    @SerializedName("timeModified")
    var timeModified: DateTime
)