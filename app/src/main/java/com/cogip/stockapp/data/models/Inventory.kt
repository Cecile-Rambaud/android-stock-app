package com.cogip.stockapp.data.models

import com.cogip.stockapp.data.responses.InventoryResponse
import com.google.gson.annotations.SerializedName

data class Inventory  (

    @SerializedName("id")
    var id: Int,

    @SerializedName("startDate")
    var startDate: DateTime?,

    @SerializedName("endDate")
    var endDate: DateTime?,

    @SerializedName("state")
    var state: Int
)  {
    constructor(inventoryApiResponse: InventoryResponse) : this(
        inventoryApiResponse.id,
        inventoryApiResponse.startDate,
        inventoryApiResponse.endDate,
        inventoryApiResponse.state
    )
}