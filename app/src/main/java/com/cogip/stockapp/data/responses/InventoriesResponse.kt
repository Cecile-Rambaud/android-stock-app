package com.cogip.stockapp.data.responses

import com.cogip.stockapp.data.models.Inventory
import com.google.gson.annotations.SerializedName

data class InventoriesResponse (

    @SerializedName("hydra:member")
    var inventories: List<Inventory>
)
