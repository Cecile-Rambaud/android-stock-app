package com.cogip.stockapp.data.responses

import com.google.gson.annotations.SerializedName
import com.cogip.stockapp.data.models.Product

data class ProductsResponse (
    @SerializedName("@id")
    var id: String, //must be "/api/products"

    @SerializedName("@context")
    var context: String, //must be "/api/contexts/Product"

    @SerializedName("hydra:totalItems")
    var totalItems: Int,

    @SerializedName("hydra:member")
    var products: List<Product>
)
