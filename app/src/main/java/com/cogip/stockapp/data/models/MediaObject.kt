package com.cogip.stockapp.data.models

import com.google.gson.annotations.SerializedName

data class MediaObject (
    @SerializedName("id")
    var id: Int,

    @SerializedName("contentUrl")
    var contentUrl: String,
)