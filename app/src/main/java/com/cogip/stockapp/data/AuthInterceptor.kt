package com.cogip.stockapp.data

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.cogip.stockapp.ui.MessageBroadcastReceiver
import com.cogip.stockapp.utils.SessionManager
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response


/**
 * Interceptor to add auth token to requests
 */
class AuthInterceptor(_sessionManager: SessionManager, _context: Context) : Interceptor {
    private var context: Context = _context
    private val sessionManager : SessionManager = _sessionManager

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()
        val request: Request = chain.request()

        if (request.url.encodedPath != "/authentication_token") {
            // If token has been saved, add it to the request
            sessionManager.fetchAuthToken()?.let {
                requestBuilder.addHeader("Authorization", "Bearer $it")
            }
        }

        val response = chain.proceed(requestBuilder.build())

        if (!response.isSuccessful) {
            // error case
            when (response.code) {
                404 -> displayErrorActivityWithMessage("Page introuvable : " + response.message)
                403 -> {
                    displayErrorActivityWithMessage("Accès refusé : " + response.message)
                }
                401 -> {
                    sessionManager.removeTokens()
                    displayErrorActivityWithMessage("Problème d'authentification, veuillez vous re-coonecter : " + response.message)
                }
                500 -> displayErrorActivityWithMessage("Erreur interne du serveur : " + response.message)
                else -> displayErrorActivityWithMessage("Erreur inconnue ("+ response.code +") : " + response.message)
            }
        }


        return response
    }



    private fun displayErrorActivityWithMessage(message : String)  {
        val receiver = MessageBroadcastReceiver()
        val manager = LocalBroadcastManager.getInstance(context)
        manager.registerReceiver(receiver, IntentFilter(MessageBroadcastReceiver.ACTION_MESSAGE_TO_DISPLAY))
        val intent = Intent(MessageBroadcastReceiver.ACTION_MESSAGE_TO_DISPLAY)
        intent.putExtra(MessageBroadcastReceiver.EXTRA_MESSAGE, message)
        manager.sendBroadcast(intent)
        manager.unregisterReceiver(receiver)
    }
}